CREATE TABLE "Alert" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    location point,
    status text,
    "isGlobal" boolean,
    photo text,
    audience jsonb,
    "totalPatrolUsers" double precision,
    "chatRoom" text,
    address text,
    "problemType" text,
    media text,
    note text,
    owner text,
    _rperm text[],
    _wperm text[]
);
ALTER TABLE ONLY "Alert"
    ADD CONSTRAINT "Alert_pkey" PRIMARY KEY ("objectId");
