CREATE TABLE "Response" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    note text,
    "forwardedBy" text,
    "travelTime" text,
    received boolean,
    seen boolean,
    "canHelp" boolean,
    alert text,
    owner text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "Response" OWNER TO parse;
--
-- Name: Response Response_pkey; Type: CONSTRAINT; Schema: public; Owner: parse
--

ALTER TABLE ONLY "Response"
    ADD CONSTRAINT "Response_pkey" PRIMARY KEY ("objectId");

