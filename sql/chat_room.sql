CREATE TABLE "ChatRoom" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    entity text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "ChatRoom" OWNER TO parse;
ALTER TABLE ONLY "ChatRoom"
    ADD CONSTRAINT "ChatRoom_pkey" PRIMARY KEY ("objectId");

