--
-- Name: ChatMsg; Type: TABLE; Schema: public; Owner: parse
--

CREATE TABLE "ChatMsg" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "chatRoom" text,
    owner text,
    text text,
    image text,
    location point,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "ChatMsg" OWNER TO parse;
ALTER TABLE ONLY "ChatMsg"
    ADD CONSTRAINT "ChatMsg_pkey" PRIMARY KEY ("objectId");
