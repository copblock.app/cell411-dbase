CREATE TABLE "PrivateCell" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    name text,
    type double precision,
    owner text,
    "chatRoom" text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "PrivateCell" OWNER TO parse;
ALTER TABLE ONLY "PrivateCell"
    ADD CONSTRAINT "PrivateCell_pkey" PRIMARY KEY ("objectId");

CREATE TABLE "_Join:members:PrivateCell" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:members:PrivateCell" OWNER TO parse;
ALTER TABLE ONLY "_Join:members:PrivateCell"
    ADD CONSTRAINT "_Join:members:PrivateCell_pkey" PRIMARY KEY ("relatedId", "owningId");
