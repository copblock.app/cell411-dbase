CREATE TABLE "Update" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    op text,
    owner text,
    "owningId" text,
    "relatedId" text,
    "owningClass" text,
    "owningField" text,
    "relatedClass" text,
    "relatedField" text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "Update" OWNER TO nn;

ALTER TABLE ONLY "Update"
    ADD CONSTRAINT "Update_pkey" PRIMARY KEY ("objectId");

