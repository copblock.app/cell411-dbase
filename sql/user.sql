CREATE TABLE "_User" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    username text,
    email text,
    "emailVerified" boolean,
    "authData" jsonb,
    _rperm text[],
    _wperm text[],
    _hashed_password text,
    _email_verify_token_expires_at timestamp with time zone,
    _email_verify_token text,
    _account_lockout_expires_at timestamp with time zone,
    _failed_login_count double precision,
    _perishable_token text,
    _perishable_token_expires_at timestamp with time zone,
    _password_changed_at timestamp with time zone,
    _password_history jsonb,
    "avatarURL" text,
    "lastName" text,
    "mobileNumber" text,
    "emergencyContactName" text,
    "firstName" text,
    "emergencyContactNumber" text,
    location point,
    "newPublicCellAlert" boolean,
    privilege text,
    "otherMedicalConditions" text,
    allergies text,
    "phoneVerified" boolean,
    "bloodType" text,
    "patrolMode" boolean,
    "thumbNail" text,
    "thumbNailURL" text,
    consented boolean
);


ALTER TABLE "_User" OWNER TO parse;

ALTER TABLE ONLY "_User"
    ADD CONSTRAINT "_User_pkey" PRIMARY KEY ("objectId");
CREATE UNIQUE INDEX "_User_unique_email" ON "_User" USING btree (email);
CREATE UNIQUE INDEX "_User_unique_username" ON "_User" USING btree (username);
CREATE INDEX case_insensitive_email ON "_User" USING btree (lower(email) varchar_pattern_ops);
CREATE INDEX case_insensitive_username ON "_User" USING btree (lower(username) varchar_pattern_ops);
CREATE TABLE "_Join:friends:_User" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:friends:_User" OWNER TO parse;

ALTER TABLE ONLY "_Join:friends:_User"
    ADD CONSTRAINT "_Join:friends:_User_pkey" PRIMARY KEY ("relatedId", "owningId");
CREATE TABLE "_Join:spammedBy:_User" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:spammedBy:_User" OWNER TO parse;

ALTER TABLE ONLY "_Join:spammedBy:_User"
    ADD CONSTRAINT "_Join:spammedBy:_User_pkey" PRIMARY KEY ("relatedId", "owningId");
CREATE TABLE "_Join:spamUsers:_User" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:spamUsers:_User" OWNER TO parse;

ALTER TABLE ONLY "_Join:spamUsers:_User"
    ADD CONSTRAINT "_Join:spamUsers:_User_pkey" PRIMARY KEY ("relatedId", "owningId");
CREATE TABLE "_Join:friends:_User" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:friends:_User" OWNER TO parse;

ALTER TABLE ONLY "_Join:friends:_User"
    ADD CONSTRAINT "_Join:friends:_User_pkey" PRIMARY KEY ("relatedId", "owningId");
CREATE TABLE "_Join:spammedBy:_User" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:spammedBy:_User" OWNER TO parse;

ALTER TABLE ONLY "_Join:spammedBy:_User"
    ADD CONSTRAINT "_Join:spammedBy:_User_pkey" PRIMARY KEY ("relatedId", "owningId");
CREATE TABLE "_Join:spamUsers:_User" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:spamUsers:_User" OWNER TO parse;

ALTER TABLE ONLY "_Join:spamUsers:_User"
    ADD CONSTRAINT "_Join:spamUsers:_User_pkey" PRIMARY KEY ("relatedId", "owningId");
