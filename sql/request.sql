CREATE TABLE "Request" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    status text,
    "sentTo" text,
    owner text,
    cell text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "Request" OWNER TO parse;

ALTER TABLE ONLY "Request"
    ADD CONSTRAINT "Request_pkey" PRIMARY KEY ("objectId");
