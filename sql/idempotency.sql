CREATE TABLE "_Idempotency" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    "reqId" text,
    expire timestamp with time zone,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "_Idempotency" OWNER TO parse;

ALTER TABLE ONLY "_Idempotency"
    ADD CONSTRAINT "_Idempotency_pkey" PRIMARY KEY ("objectId");
CREATE UNIQUE INDEX "_Idempotency_unique_reqId" ON "_Idempotency" USING btree ("reqId");
CREATE INDEX ttl ON "_Idempotency" USING btree (expire);
