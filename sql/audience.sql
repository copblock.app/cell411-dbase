CREATE TABLE "_Audience" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    name text,
    query text,
    "lastUsed" timestamp with time zone,
    "timesUsed" double precision,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "_Audience" OWNER TO parse;

ALTER TABLE ONLY "_Audience"
    ADD CONSTRAINT "_Audience_pkey" PRIMARY KEY ("objectId");

