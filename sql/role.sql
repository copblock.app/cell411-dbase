CREATE TABLE "_Role" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    name text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "_Role" OWNER TO parse;

ALTER TABLE ONLY "_Role"
    ADD CONSTRAINT "_Role_pkey" PRIMARY KEY ("objectId");
-- Name: _Role_unique_name; Type: INDEX; Schema: public; Owner: parse
CREATE UNIQUE INDEX "_Role_unique_name" ON "_Role" USING btree (name);
CREATE TABLE "_Join:roles:_Role" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:roles:_Role" OWNER TO parse;

ALTER TABLE ONLY "_Join:roles:_Role"
    ADD CONSTRAINT "_Join:roles:_Role_pkey" PRIMARY KEY ("relatedId", "owningId");

CREATE TABLE "_Join:users:_Role" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:users:_Role" OWNER TO parse;

ALTER TABLE ONLY "_Join:users:_Role"
    ADD CONSTRAINT "_Join:users:_Role_pkey" PRIMARY KEY ("relatedId", "owningId");
