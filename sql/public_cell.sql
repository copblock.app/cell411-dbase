CREATE TABLE "PublicCell" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    name text,
    "isVerified" boolean,
    description text,
    category text,
    "verificationStatus" double precision,
    "cellType" double precision,
    "chatRoom" text,
    location point,
    owner text,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "PublicCell" OWNER TO parse;
ALTER TABLE ONLY "PublicCell"
    ADD CONSTRAINT "PublicCell_pkey" PRIMARY KEY ("objectId");

CREATE TABLE "_Join:members:PublicCell" (
    "relatedId" character varying(120) NOT NULL,
    "owningId" character varying(120) NOT NULL
);


ALTER TABLE "_Join:members:PublicCell" OWNER TO parse;

ALTER TABLE ONLY "_Join:members:PublicCell"
    ADD CONSTRAINT "_Join:members:PublicCell_pkey" PRIMARY KEY ("relatedId", "owningId");
