
CREATE TABLE "PrivacyPolicy" (
    "objectId" text NOT NULL,
    "createdAt" timestamp with time zone,
    "updatedAt" timestamp with time zone,
    version text,
    "versionCode" double precision,
    _rperm text[],
    _wperm text[]
);


ALTER TABLE "PrivacyPolicy" OWNER TO parse;

ALTER TABLE ONLY "PrivacyPolicy"
    ADD CONSTRAINT "PrivacyPolicy_pkey" PRIMARY KEY ("objectId");

